#md5hash Extended

This is an update of wraithdu's md5hash - once upon a time it was available on the portableapps.com forums.  I wanted the program to compare hashes, which wraithdu told me to implement.  So I buggered up the autoit code enough to have it do that.

##Update
So, I discovered a copy of md5Hash hiding on a flash drive.  I had to download and install AutoIt v3.3.4.0 to compile this.  Its x86, so 64 bit folks need not apply.  This is an old program from 2010 (funny to say that 2010 is old) but I believe it was and is the best hashing program I ever found.

This version is patched as a *.paf.exe installer so it can be used with the PortableApps.com menu.
