#Include "windows.bi"
' declare function pointer
Dim Shared HashFile As Function (ByVal szType As ZString Ptr, ByVal szFile As WString Ptr, ByVal szOut As Any Ptr, ByVal hProgress As HWND) As Long
Dim Shared As Any Ptr hMutex, pHash = 0
' global data structure
Type mythreadinfo
	szType As ZString * 20
	szFile As WString * 260
	szOut As Any Ptr
	hProgress As HWND
End Type
Dim Shared myinfo As mythreadinfo
' worker thread
Sub HashFileNow()
	' make local copy of structure, unlock mutex, and call function
	Dim lInfo As mythreadinfo = myinfo
	MutexUnLock(hMutex)
	HashFile(lInfo.szType, lInfo.szFile, lInfo.szOut, lInfo.hProgress)
End Sub

Function HashFileThread Cdecl Alias "HashFileThread" (ByVal szType As ZString Ptr , ByVal szFile As WString Ptr, ByVal szOut As Any Ptr, ByVal hProgress As HWND) As Any Ptr Export
	If pHash = 0 Then
		' load hash.dll and create mutex on first call
		pHash = DylibLoad("hash.dll")
		If pHash = 0 Then Return NULL
		HashFile = DylibSymbol(pHash, "hashfile")
		If HashFile = 0 Then
			DylibFree(pHash)
			pHash = 0
			Return NULL
		EndIf
		hMutex = MutexCreate()
	EndIf
	MutexLock(hMutex)
	myinfo.szType = *szType
	myinfo.szFile = *szFile
	myinfo.szOut = szOut
	myinfo.hProgress = hProgress
	' use the WinAPI CreateThread so we get a valid thread handle
	Return CreateThread(NULL, 0, Cast(Any Ptr, @HashFileNow), NULL, 0, NULL)
End Function

Sub _OnExit() Destructor
	' free resources
	DylibFree(pHash)
	MutexDestroy(hMutex)
End Sub