md5hash Launcher 1.0.0.0
========================
Copyright 2009-2010 Erik Pilsits
Copyright 2004-2009 John T. Haller

Website: http://zer0dev.com

This software is OSI Certified Open Source Software.
OSI Certified is a certification mark of the Open Source Initiative.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


About md5hash
=============
The md5hash Launcher allows you to run md5hash from a removable drive
whose letter changes as you move it to another computer.  The program can be
entirely self-contained on the drive and then used on any Windows computer.


License
=======
This code is released under the GPL.  Within the md5hash Source directory
you will find the code (md5hash.nsi) as well as the full GPL license
(License.txt).  If you use the launcher or code in your own product, please give
proper and prominent attribution.


Installation / Directory Structure
==================================
By default, the program expects this directory structure:

-\ <--- Directory with md5hash.exe
	+\App\
		+\md5hash\
	+\Data\


md5hash.ini Configuration
=========================
The md5hash Launcher will look for an ini file called
md5hash.ini (read the previous section for details on placement).  If
you are happy with the default options, it is not necessary, though.  The INI
file is formatted as follows:

[md5hash]
DisableSplashScreen=false

The DisableSplashScreen entry allows you to run the md5hash Launcher
without the splash screen showing up.  The default is false.