;Copyright (C) 2004-2009 John T. Haller of PortableApps.com
;Copyright (C) 2007-2008 Ryan McCue of PortableApps.com
;Copyright (C) 2009-2010 Erik Pilsits

;Website: http://PortableApps.com

;This software is OSI Certified Open Source Software.
;OSI Certified is a certification mark of the Open Source Initiative.

;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either version 2
;of the License, or (at your option) any later version.

;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.

;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

!define PORTABLEAPPNAME "md5hash"
!define APPNAME "md5hash"
!define NAME "md5hash"
!define VER "1.0.0.0"
!define WEBSITE "zer0dev.com"
!define DEFAULTEXE "md5hash.exe"
!define DEFAULTAPPDIR "md5hash"
;!define DEFAULTSETTINGSDIR "settings"
!define LAUNCHERLANGUAGE "English"

;=== Program Details
Name "${PORTABLEAPPNAME}"
OutFile "..\..\${NAME}.exe"
Caption "${PORTABLEAPPNAME}"
VIProductVersion "${VER}"
VIAddVersionKey ProductName "${PORTABLEAPPNAME}"
VIAddVersionKey Comments "Allows ${APPNAME} to be run from a removable drive.  For additional details, visit ${WEBSITE}"
VIAddVersionKey CompanyName "zer0dev.com"
VIAddVersionKey LegalCopyright "Erik Pilsits"
VIAddVersionKey FileDescription "${PORTABLEAPPNAME}"
VIAddVersionKey FileVersion "${VER}"
VIAddVersionKey ProductVersion "${VER}"
VIAddVersionKey InternalName "${PORTABLEAPPNAME}"
;VIAddVersionKey LegalTrademarks "PortableApps.com is a Trademark of Rare Ideas, LLC."
VIAddVersionKey OriginalFilename "${NAME}.exe"
;VIAddVersionKey PrivateBuild ""
;VIAddVersionKey SpecialBuild ""

;=== Runtime Switches
CRCCheck On
WindowIcon Off
SilentInstall Silent
AutoCloseWindow True
RequestExecutionLevel user

; Best Compression
SetCompress Auto
SetCompressor /SOLID lzma
SetCompressorDictSize 32
SetDatablockOptimize On

;=== Include
;(Standard NSIS)
;!include Registry.nsh
!include FileFunc.nsh
;!insertmacro GetRoot
!insertmacro GetParameters
;!include LogicLib.nsh
;!include WinVer.nsh

;(NSIS Plugins)
;!include TextReplace.nsh

;(Custom)
;!include ReplaceInFileWithTextReplace.nsh
!include ReadINIStrWithDefault.nsh

;=== Program Icon
Icon "..\..\App\AppInfo\appicon.ico"

;=== Icon & Stye ===
!define MUI_ICON "..\..\App\AppInfo\appicon.ico"

;=== Languages
LoadLanguageFile "${NSISDIR}\Contrib\Language files\${LAUNCHERLANGUAGE}.nlf"
!include PortableApps.comLauncherLANG_${LAUNCHERLANGUAGE}.nsh

Var PROGRAMDIRECTORY
Var SETTINGSDIRECTORY
;Var ADDITIONALPARAMETERS
Var EXECSTRING
Var SECONDARYLAUNCH
Var DISABLESPLASHSCREEN
;Var FAILEDTORESTOREKEY
Var MISSINGFILEORPATH
;Var APPLANGUAGE


Section "Main"
	;=== Check if already running
	System::Call 'kernel32::CreateMutexW(i 0, i 0, w "md5hashSingleton") i .r1 ?e'
	Pop $R0
	StrCmp $R0 "0" CheckINI
		StrCpy $SECONDARYLAUNCH "true"

	CheckINI:
		System::Call 'kernel32::CloseHandle(i $1)' ; close the mutex handle
		${ReadINIStrWithDefault} $DISABLESPLASHSCREEN "$EXEDIR\${NAME}.ini" "${NAME}" "DisableSplashScreen" "false"
		StrCpy $PROGRAMDIRECTORY "$EXEDIR\App\${DEFAULTAPPDIR}"
		StrCpy $SETTINGSDIRECTORY "$EXEDIR\Data"

		IfFileExists "$PROGRAMDIRECTORY\${DEFAULTEXE}" FoundProgramEXE

	;NoProgramEXE:
		;=== Program executable not where expected
		StrCpy $MISSINGFILEORPATH ${DEFAULTEXE}
		MessageBox MB_OK|MB_ICONEXCLAMATION `$(LauncherFileNotFound)`
		Abort

	FoundProgramEXE:
		;=== Check if running
		StrCmp $SECONDARYLAUNCH "true" MakeEXEString

	;DisplaySplash:
		StrCmp $DISABLESPLASHSCREEN "true" MakeEXEString
			;=== Show the splash screen while processing registry entries
			InitPluginsDir
			File /oname=$PLUGINSDIR\splash.jpg "${NAME}.jpg"
			newadvsplash::show /NOUNLOAD 1200 0 0 -1 /L $PLUGINSDIR\splash.jpg

	MakeEXEString:
		StrCpy $EXECSTRING `"$PROGRAMDIRECTORY\${DEFAULTEXE}"`

	;GetPassedParameters:
		;=== Get any passed parameters
		${GetParameters} $0
		StrCmp "'$0'" "''" SettingsDirectory
		StrCpy $EXECSTRING `$EXECSTRING $0`

	SettingsDirectory:
		;=== Set the settings directory if we have a path
		IfFileExists "$SETTINGSDIRECTORY\*.*" LaunchAndExit
			CreateDirectory $SETTINGSDIRECTORY

	LaunchAndExit:
		Exec $EXECSTRING

	;TheEnd:
		newadvsplash::stop /WAIT
SectionEnd