md5hash
=======

md5hash is a tool for calculating file checksums.

Features:
- select from 11 hashing algorithms
- UPPERCASE or lowercase hash output
- optional progress bar to track long operations
- always on top toggle
- auto-copies single file's hash to clipboard
- session history for reviewing hashes
- easy dbl-click copy of session data to clipboard
- right-click to save or clear session data
- drag-and-drop support for any number of files
- drop two files, and an automatic comparison is done
- easy integration into the shell context menu (.reg file included, edit path to suit)

All options are available from the GUI's system menu.  The INI is created on first run if it does
not exist.

NOTE:
If the progress bar is toggled to show after a file operation has already begun, it will not
be updated until the next file is hashed.  This is done for maximum efficiency during file hashing
when the progress bar is initially hidden.


Context Menu Integration
========================

As part of a permanent installation, md5hash can easily be integrated into the default system
context menu for files and directories.  Simply edit the included 'context.reg' file to point to
the location of 'md5hash.exe' (be mindful of the \\ syntax), save it, then double-click it to
merge it into your registry.


Changelog
=========

1.0.3.5
- Fixed open handles in hash.dll
- Added recursion into folders, configurable depth setting
- Added optional warning for large hashing jobs (>500 files or >1 GB of data)
- Added command to manually compare last two hashed files

1.0.3.4
- Session list view is now sortable
- Session allows multi-select for saving and removal

1.0.3.3
- Added Always On Top toggle
- Shrunk progress bar height to 8px

1.0.3.2
- Added Unicode support
- Added 256x256 icon